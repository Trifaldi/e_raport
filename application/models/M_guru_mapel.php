<?php 
/**
 * 
 */
class M_guru_mapel extends CI_Model
{
	function list(){
		$this->db->select('guru_mapel.id,guru.nip,guru.nama,mata_pelajaran.nama_mapel,mata_pelajaran.id AS mapel');
		$this->db->from('guru_mapel');
		$this->db->join('mata_pelajaran', 'mata_pelajaran.id = guru_mapel.id_mapel');
		$this->db->join('guru', 'guru.nip = guru_mapel.nip_guru');
		$data = $this->db->get();
		return $data->result();
	}

	function simpan(){
		$data = array( 
				'id_mapel' 	=> $this->input->post('id_mapel'), 
				'nip_guru' => $this->input->post('nip'), 
			);
		$result=$this->db->insert('guru_mapel',$data);
		return $result;
	}

	function delete(){
		$id=$this->input->post('id');
		$this->db->where('id', $id);
		$result=$this->db->delete('guru_mapel');
		return $result;
	}
	
		function update(){
		$id=$this->input->post('id');
		$nip_guru=$this->input->post('nip');
		$id_mapel=$this->input->post('id_mapel');

		$this->db->set('nip_guru', $nip_guru);
		$this->db->set('id_mapel', $id_mapel);
		$this->db->where('id', $id);
		$result=$this->db->update('guru_mapel');
		return $result;
	}

	function get_nip($nip){
        $query = $this->db->get_where('guru', array('nip' => $nip));
        return $query;
    }

    function get_mapel($mapel){
        $query = $this->db->get_where('mata_pelajaran', array('id' => $mapel));
        return $query;
    }
}