<?php 
/**
 * 
 */
class M_kelas extends CI_Model
{
	function kelas_list(){
		$data=$this->db->get('kelas');
		return $data->result();
	}

	function simpan(){
		$data = array( 
				'nama_kelas' 	=> $this->input->post('nama_kelas'), 
				'group' => $this->input->post('group'), 
			);
		$result=$this->db->insert('kelas',$data);
		return $result;
	}

	function delete_kelas(){
		$id=$this->input->post('id');
		$this->db->where('id', $id);
		$result=$this->db->delete('kelas');
		return $result;
	}
	
	function update(){
		$id=$this->input->post('id');
		$nama_kelas=$this->input->post('nama_kelas');
		$group=$this->input->post('group');

		$this->db->set('nama_kelas', $nama_kelas);
		$this->db->set('group', $group);
		$this->db->where('id', $id);
		$result=$this->db->update('kelas');
		return $result;
	}

	//Model kelas_register

	function list_kelas_register() {
		$this->db->select('a.id,b.id AS thn_ajaran,b.tanggal_awal,b.tanggal_akhir,c.id AS kelas,c.nama_kelas,c.group');
		$this->db->from('kelas_register AS a');
		$this->db->join('tahun_ajaran AS b', 'b.id = a.id_tahun_ajaran');
		$this->db->join('kelas AS c', 'c.id = a.id_kelas');
		$data = $this->db->get();
		return $data->result();
	}

	function simpan_register(){
		$data = array( 
				'id_tahun_ajaran' 	=> $this->input->post('id_tahun_ajaran'), 
				'id_kelas' => $this->input->post('id_kelas'), 
			);
		$result=$this->db->insert('kelas_register',$data);
		return $result;
	}

	function update_register(){
		$id=$this->input->post('id');
		$id_tahun_ajaran=$this->input->post('id_tahun_ajaran');
		$id_kelas=$this->input->post('id_kelas');

		$this->db->set('id_tahun_ajaran', $id_tahun_ajaran);
		$this->db->set('id_kelas', $id_kelas);
		$this->db->where('id', $id);
		$result=$this->db->update('kelas_register');
		return $result;
	}

	function delete_register(){
		$id=$this->input->post('id');
		$this->db->where('id', $id);
		$result=$this->db->delete('kelas_register');
		return $result;
	}

}