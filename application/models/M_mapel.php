<?php 
/**
 * 
 */
class M_mapel extends CI_Model
{
	function list(){
		$data=$this->db->get('mata_pelajaran');
		return $data->result();
	}

	function simpan(){
		$data = array( 
				'nama_mapel' 	=> $this->input->post('nama_mapel'), 
				'kode_mapel' => $this->input->post('kode_mapel'), 
			);
		$result=$this->db->insert('mata_pelajaran',$data);
		return $result;
	}

	function delete(){
		$id=$this->input->post('id');
		$this->db->where('id', $id);
		$result=$this->db->delete('mata_pelajaran');
		return $result;
	}
	
		function update(){
		$id=$this->input->post('id');
		$nama_mapel=$this->input->post('nama_mapel');
		$kode_mapel=$this->input->post('kode_mapel');

		$this->db->set('nama_mapel', $nama_mapel);
		$this->db->set('kode_mapel', $kode_mapel);
		$this->db->where('id', $id);
		$result=$this->db->update('mata_pelajaran');
		return $result;
	}
}