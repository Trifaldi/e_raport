<?php 
/**
 * 
 */
class M_thn_ajaran extends CI_Model
{
	function list(){
		$data=$this->db->get('tahun_ajaran');
		return $data->result();
	}

	function simpan(){
		$data = array( 
				'tanggal_awal' 	=> $this->input->post('tanggal_awal'), 
				'tanggal_akhir' => $this->input->post('tanggal_akhir'), 
			);
		$result=$this->db->insert('tahun_ajaran',$data);
		return $result;
	}

	function delete(){
		$id=$this->input->post('id');
		$this->db->where('id', $id);
		$result=$this->db->delete('tahun_ajaran');
		return $result;
	}
	
		function update(){
		$id=$this->input->post('id');
		$tanggal_awal=$this->input->post('tanggal_awal');
		$tanggal_akhir=$this->input->post('tanggal_akhir');

		$this->db->set('tanggal_awal', $tanggal_awal);
		$this->db->set('tanggal_akhir', $tanggal_akhir);
		$this->db->where('id', $id);
		$result=$this->db->update('tahun_ajaran');
		return $result;
	}
}