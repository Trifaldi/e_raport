<?php 
/**
 * 
 */
class M_guru extends CI_Model
{
	function list(){
		$data=$this->db->get('guru');
		return $data->result();
	}

	function simpan(){
		$data = array( 
				'nama' 	=> $this->input->post('nama'), 
				'email' => $this->input->post('email'),
				'tanggal_lahir' 	=> $this->input->post('tanggal_lahir'), 
				'tempat_lahir' => $this->input->post('tempat_lahir'),
				'alamat' => $this->input->post('alamat'),
				'golongan' 	=> $this->input->post('golongan'), 
				'nip' => $this->input->post('nip'),
				'jenis_kelamin' 	=> $this->input->post('jenis_kelamin'), 
				'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir'),
				'telfon' 	=> $this->input->post('telfon'), 
				'tanggal_sk' => $this->input->post('tanggal_sk'),
				'username' => $this->input->post('nip'),
				'password' => md5($this->input->post('nip')), 
			);
		$result=$this->db->insert('guru',$data);

		$user_login = array(
			'username' => $this->input->post('nip'),
			'password' => md5($this->input->post('nip')),
			'level' => 'guru'
		);

		$result=$this->db->insert('users',$user_login);

		return $result;
	}

	function delete(){
		$id=$this->input->post('id');
		$this->db->where('id', $id);
		$result=$this->db->delete('guru');
		return $result;
	}
	
		function update(){
		$id=$this->input->post('id');
		$nama=$this->input->post('nama');
		$email=$this->input->post('email');
		$tanggal_lahir=$this->input->post('tanggal_lahir');
		$tempat_lahir=$this->input->post('tempat_lahir');
		$agama=$this->input->post('agama');
		$alamat=$this->input->post('alamat');
		$golongan=$this->input->post('golongan');
		$nip=$this->input->post('nip');
		$jenis_kelamin=$this->input->post('jenis_kelamin');
		$pendidikan_terakhir=$this->input->post('pendidikan_terakhir');
		$telfon=$this->input->post('telfon');
		$tanggal_sk=$this->input->post('tanggal_sk');

		$this->db->set('nama', $nama);
		$this->db->set('email', $email);
		$this->db->set('tanggal_lahir', $tanggal_lahir);
		$this->db->set('tempat_lahir', $tempat_lahir);
		$this->db->set('agama', $agama);
		$this->db->set('alamat', $alamat);
		$this->db->set('golongan', $golongan);
		$this->db->set('nip', $nip);
		$this->db->set('jenis_kelamin', $jenis_kelamin);
		$this->db->set('pendidikan_terakhir', $pendidikan_terakhir);
		$this->db->set('telfon', $telfon);
		$this->db->set('tanggal_sk', $tanggal_sk);
		$this->db->where('id', $id);
		$result=$this->db->update('guru');
		return $result;
	}
}