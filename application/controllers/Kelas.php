<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends AUTH_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_kelas');
	}

	public function index() {
		$data = array(
			'userdata' => $this->userdata,
			'title' => "Kelas",
			'page' => "kelas"
		);
		$this->load->view('kelas/index', $data);
	}

	function data_kelas(){
		$data=$this->m_kelas->kelas_list();
		echo json_encode($data);
	}

	function save(){
		$data=$this->m_kelas->simpan();
		echo json_encode($data);
	}

	function update(){
		$data=$this->m_kelas->update();
		echo json_encode($data);
	}

	function delete(){
		$data=$this->m_kelas->delete_kelas();
		echo json_encode($data);
	}
}