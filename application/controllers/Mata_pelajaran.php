<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mata_pelajaran extends AUTH_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_mapel');
	}

	public function index() {
		$data = array(
			'userdata' => $this->userdata,
			'title' => "Mata pelajaran",
			'page' => "mata_pelajaran"
		);
		$this->load->view('mapel/index', $data);
	}

	function list(){
		$data=$this->m_mapel->list();
		echo json_encode($data);
	}

	function save(){
		$data=$this->m_mapel->simpan();
		echo json_encode($data);
	}

	function update(){
		$data=$this->m_mapel->update();
		echo json_encode($data);
	}

	function delete(){
		$data=$this->m_mapel->delete();
		echo json_encode($data);
	}
}