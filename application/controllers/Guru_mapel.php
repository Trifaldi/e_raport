<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guru_mapel extends AUTH_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_guru_mapel');
		$this->load->model('m_guru');
		$this->load->model('m_mapel');
	}

	public function index() {

		$data = array(
			'userdata' => $this->userdata,
			'guru' => $this->db->query('select * from guru')->result(),
			'mapel' => $this->db->query('select * from mata_pelajaran')->result(),
			'title' => "Guru Mapel",
			'page' => "guru_mapel"
		);
		$this->load->view('guru_mapel/index', $data);
	}

	function list(){
		$data=$this->m_guru_mapel->list();
		echo json_encode($data);
	}

	function save(){
		$data=$this->m_guru_mapel->simpan();
		echo json_encode($data);
	}

	function update(){
		$data=$this->m_guru_mapel->update();
		echo json_encode($data);
	}

	function delete(){
		$data=$this->m_guru_mapel->delete();
		echo json_encode($data);
	}

	function get_nip(){
        $data = $this->m_guru->list();
        echo json_encode($data);
	}

	function get_mapel(){
        $data = $this->m_mapel->list();
        echo json_encode($data);
	}
}