<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends AUTH_Controller {

	public function index() {
		//$data['userdata'] 		= $this->userdata;
		$data = array(
			'userdata' => $this->userdata,
			'title' => "Home",
			'page' => "home"
		);

		if ($this->userdata->level == 'admin') {

			$this->load->view('index', $data);
		} else {
			$this->load->view('siswa', $data);
		}
		
	}
}