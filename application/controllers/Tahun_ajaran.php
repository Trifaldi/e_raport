<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tahun_ajaran extends AUTH_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_thn_ajaran');
	}

	public function index() {
		$data = array(
			'userdata' => $this->userdata,
			'title' => "Tahun_ajaran",
			'page' => "tahun_ajaran"
		);
		$this->load->view('tahun_ajaran/index', $data);
	}

	function list(){
		$data=$this->m_thn_ajaran->list();
		echo json_encode($data);
	}

	function save(){
		$data=$this->m_thn_ajaran->simpan();
		echo json_encode($data);
	}

	function update(){
		$data=$this->m_thn_ajaran->update();
		echo json_encode($data);
	}

	function delete(){
		$data=$this->m_thn_ajaran->delete();
		echo json_encode($data);
	}
}