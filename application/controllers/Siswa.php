<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends AUTH_Controller {

	public function index() {
		$data = array(
			'userdata' => $this->userdata,
			'title' => "Siswa",
			'page' => "siswa"
		);
		$this->load->view('siswa', $data);
	}
}