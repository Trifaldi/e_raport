<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guru extends AUTH_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_guru');
	}

	public function index() {
		$data = array(
			'userdata' => $this->userdata,
			'title' => "Guru",
			'page' => "guru"
		);
		$this->load->view('guru/index', $data);
	}

	function list(){
		$data=$this->m_guru->list();
		echo json_encode($data);
	}

	function save(){
		$data=$this->m_guru->simpan();
		echo json_encode($data);
	}

	function update(){
		$data=$this->m_guru->update();
		echo json_encode($data);
	}

	function delete(){
		$data=$this->m_guru->delete();
		echo json_encode($data);
	}
}