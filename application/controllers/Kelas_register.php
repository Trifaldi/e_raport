<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas_register extends AUTH_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_kelas');
		$this->load->model('m_thn_ajaran');
	}

	public function index() {
		$data = array(
			'userdata' => $this->userdata,
			'tahun_ajaran' => $this->db->query('select * from tahun_ajaran')->result(),
			'kelas' => $this->db->query('select * from kelas')->result(),
			'title' => "Kelas Register",
			'page' => "kelas_register"
		);
		$this->load->view('kelas/register', $data);
	}

	function data_kelas(){
		$data=$this->m_kelas->list_kelas_register();
		echo json_encode($data);
	}

	function save(){
		$data=$this->m_kelas->simpan_register();
		echo json_encode($data);
	}

	function update(){
		$data=$this->m_kelas->update_register();
		echo json_encode($data);
	}

	function delete(){
		$data=$this->m_kelas->delete_register();
		echo json_encode($data);
	}

	function get_tahun_ajaran(){
        $data = $this->m_thn_ajaran->list();
        echo json_encode($data);
	}

	function get_kelas(){
        $data = $this->m_kelas->kelas_list();
        echo json_encode($data);
	}
}