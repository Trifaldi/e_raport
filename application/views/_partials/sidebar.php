<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
      <div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="<?php echo base_url(); ?>dist/index">Stisla</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="<?php echo base_url(); ?>dist/index">St</a>
          </div>
          <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="dropdown <?php echo $page == 'home' ? 'active' : ''; ?>">
              <a href="<?php echo base_url(); ?>" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
            </li>
            <li class="menu-header">Data Master</li>
            <li class="dropdown <?php echo $page == 'tahun_ajaran' || $page == 'kelas' || $page == 'mata_pelajaran' ? 'active' : ''; ?>">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-plug"></i> <span>Data Master</span></a>
              <ul class="dropdown-menu">
                <li class="<?php echo $page == 'kelas' ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url(); ?>kelas">Kelas</a></li>
                <li class="<?php echo $page == 'mata_pelajaran' ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url(); ?>mata_pelajaran">Mata Pelajaran</a></li>
                <li class="<?php echo $page == 'tahun_ajaran' ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url(); ?>tahun_ajaran">Tahun Ajaran</a></li>
              </ul>
            </li>
            <li class="menu-header">Data</li>
            <li class="dropdown <?php echo $page == 'siswa' ? 'active' : ''; ?>">
              <a href="<?php echo base_url(); ?>siswa" class="nav-link"><i class="fas fa-user"></i> <span>Siswa</span></a>
            </li>
            <li class="dropdown <?php echo $page == 'guru' ? 'active' : ''; ?>">
              <a href="<?php echo base_url(); ?>guru" class="nav-link"><i class="fas fa-user"></i> <span>Guru</span></a>
            </li>

            <li class="menu-header">Seting</li>
            <li class="dropdown <?php echo $page == 'kelas_register' ? 'active' : ''; ?>">
              <a href="<?php echo base_url(); ?>kelas_register" class="nav-link"><i class="fas fa-user"></i> <span>Kelas Register</span></a>
            </li>
            <li class="dropdown <?php echo $page == 'wali_kelas' ? 'active' : ''; ?>">
              <a href="<?php echo base_url(); ?>wali_kelas" class="nav-link"><i class="fas fa-user"></i> <span>Wali Kelas</span></a>
            </li>
            <li class="dropdown <?php echo $page == 'guru_mapel' ? 'active' : ''; ?>">
              <a href="<?php echo base_url(); ?>guru_mapel" class="nav-link"><i class="fas fa-user"></i> <span>Guru Mapel</span></a>
            </li>
           
        </aside>
      </div>
