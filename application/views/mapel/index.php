<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('_partials/header');
?>
 <!--  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/dataTables/css/jquery.dataTables.css'?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/dataTables/css/dataTables.bootstrap4.css'?>"> -->
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Mata Pelajaran</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
              <div class="breadcrumb-item"><a href="#">Data Master</a></div>
              <div class="breadcrumb-item">Mata Pelajaran</div>
            </div>
          </div>

          <div class="section-body">
            <a href="#" data-toggle="modal" data-target="#Modal_Add" class="btn btn-icon icon-left btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>

            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped" id="table-1">
                        <thead>                                 
                          <tr>
                            <th class="text-center">
                              #
                            </th>
                            <th>Nama Mapel</th>
                            <th>Kode Mapel</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody id="show_mapel">                                 
                          
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
          
            <div class="modal fade" id="Modal_Add" tabindex="-1" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Mata Pelajaran</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <form>
                  <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Nama Mata Pelajaran</label>
                            <div class="col-md-10">
                              <input type="text" name="nama_mapel" id="nama_mapel" class="form-control" placeholder="Nama Mapel" required>
                             <div class="invalid-feedback" id="er_nama_mapel">Nama Kelas Tidak boleh Kosong</div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Kode Mapel</label>
                            <div class="col-md-10">
                              <input type="text" name="kode_mapel" id="kode_mapel" class="form-control" placeholder="Kode Mapel" required>
                            </div>
                        </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="btn_cancel" data-dismiss="modal">Cancel</button>
                    <button type="button" type="submit" id="btn_save" class="btn btn-primary">Save</button>
                  </div>
                </form>
                </div>
              </div>
            </div>

            <!-- MODAL EDIT -->
        <form>
            <div class="modal fade" id="Modal_Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                        <input type="hidden" name="id" id="id_edit" class="form-control">
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Nama Kelas</label>
                            <div class="col-md-10">
                              <input type="text" name="nama_mapel" id="nama_mapel_edit" class="form-control" placeholder="Nama Mapel">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Group</label>
                            <div class="col-md-10">
                              <input type="text" name="kode_mapel" id="kode_mapel_edit" class="form-control" placeholder="Kode Mapel">
                            </div>
                        </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="btn_close" data-dismiss="modal">Close</button>
                    <button type="button" type="submit" id="btn_update" class="btn btn-primary">Update</button>
                  </div>
                </div>
              </div>
            </div>
            </form>
        <!--END MODAL EDIT-->



            <!--MODAL DELETE-->
         <form>
            <div class="modal fade" id="Modal_Delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                       <strong>Are you sure to delete this record?</strong>
                  </div>
                  <div class="modal-footer">
                    <input type="hidden" name="id" id="id" class="form-control">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button type="button" type="submit" id="btn_delete" class="btn btn-primary">Yes</button>
                  </div>
                </div>
              </div>
            </div>
            </form>
        <!--END MODAL DELETE-->

<script type="text/javascript" src="<?php echo base_url().'assets/dataTables/js/jquery-3.2.1.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/dataTables/js/jquery.dataTables.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/dataTables/js/dataTables.bootstrap4.js'?>"></script>

<script async='async' type="text/javascript">

    show_mapel();
    $('#btn_close').on('click',function(){
        $('[name="id"]').val('');
        $('[name="nama_mapel"]').val('');
        $('[name="kode_mapel"]').val('');
      });
    $('#btn_cancel').on('click',function(){
        $('[name="id"]').val('');
        $('[name="nama_mapel"]').val('');
        $('[name="kode_mapel"]').val('');
      });
   // $('#table-1').dataTable();

      function show_mapel(){
        $.ajax({
            type  : 'ajax',
            url   : '<?php echo site_url('mata_pelajaran/list')?>',
            async : false,
            dataType : 'json',
            success : function(data){
                var no = 1;
                var html = '';
                var i;
                for(i=0; i<data.length; i++){
                    html += '<tr>'+
                          '<td>'+ no++ +'</td>'+
                            '<td>'+data[i].nama_mapel+'</td>'+
                            '<td>'+data[i].kode_mapel+'</td>'+
                            '<td>'+
                                    '<a href="javascript:void(0);" class="btn btn-info btn-sm item_edit" data-id="'+data[i].id+'" data-nama_mapel="'+data[i].nama_mapel+'" data-kode_mapel="'+data[i].kode_mapel+'">Edit</a>'+' '+
                                    '<a href="javascript:void(0);" class="btn btn-danger btn-sm item_delete" data-id="'+data[i].id+'">Delete</a>'+
                                '</td>'+
                            '</tr>';
                }
                $('#show_mapel').html(html);
            }

        });
    }

           
        //get data for update record
        $('#show_mapel').on('click','.item_edit',function(){
            var id = $(this).data('id');
            var nama_mapel = $(this).data('nama_mapel');
            var kode_mapel        = $(this).data('kode_mapel');
            
            $('#Modal_Edit').modal('show');
            $('[name="id"]').val(id);
            $('[name="nama_mapel"]').val(nama_mapel);
            $('[name="kode_mapel"]').val(kode_mapel);
        });

        //update record to database
         $('#btn_update').on('click',function(){
            var id = $('#id_edit').val();
            var nama_mapel = $('#nama_mapel_edit').val();
            var kode_mapel = $('#kode_mapel_edit').val();
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('mata_pelajaran/update')?>",
                dataType : "JSON",
                data : {id:id , nama_mapel:nama_mapel, kode_mapel:kode_mapel},
                success: function(data){
                    $('[name="id"]').val("");
                    $('[name="nama_mapel"]').val("");
                    $('[name="kode_mapel"]').val("");
                    $('#Modal_Edit').modal('hide');
                    show_mapel();
                }
            });
            return false;
        });

        //Save product
        $('#btn_save').on('click',function(){
            var nama_mapel = $('#nama_mapel').val();
            var kode_mapel        = $('#kode_mapel').val();
            if (nama_mapel == '') {
              
            }else{
                          $.ajax({
                type : "POST",
                url  : "<?php echo site_url('mata_pelajaran/save')?>",
                dataType : "JSON",
                data : {nama_mapel:nama_mapel, kode_mapel:kode_mapel},
                success: function(data){
                    $('[name="nama_mapel"]').val("");
                    $('[name="kode_mapel"]').val("");
                    $('#Modal_Add').modal('hide');
                    show_mapel();
                }
            });
            return false;

            }

        });

        //get data for delete record
        $('#show_mapel').on('click','.item_delete',function(){
            var id_mapel = $(this).data('id');
            
            $('#Modal_Delete').modal('show');
            $('[name="id"]').val(id_mapel);
        });

        //delete record to database
         $('#btn_delete').on('click',function(){
            var id = $('#id').val();
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('mata_pelajaran/delete')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                    $('[name="id"]').val("");
                    $('#Modal_Delete').modal('hide');
                    show_mapel();
                }
            });
            return false;
        });
</script>

<?php $this->load->view('_partials/footer'); ?>