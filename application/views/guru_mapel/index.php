<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('_partials/header');
?>
 <!--  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/dataTables/css/jquery.dataTables.css'?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/dataTables/css/dataTables.bootstrap4.css'?>"> -->
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Guru Mata Pelajaran</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
              <div class="breadcrumb-item"><a href="#">Data Master</a></div>
              <div class="breadcrumb-item">Mata Pelajaran</div>
            </div>
          </div>

          <div class="section-body">
            <a href="#" data-toggle="modal" data-target="#Modal_Add" class="btn btn-icon icon-left btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>

            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped" id="table-1">
                        <thead>                                 
                          <tr>
                            <th class="text-center">
                              #
                            </th>
                            <th>Nama</th>
                            <th>NIP</th>
                            <th>Mata Pelajaran</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody id="show_list">                                 
                          
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
          
            <div class="modal fade" id="Modal_Add" tabindex="-1" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Guru Mata Pelajaran</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <form>
                  <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Nama Guru</label>
                            <div class="col-md-10">
                              <select class="form-control" name="nip" id="id_guru">
                                <option value=" ">Pilih</option>
                                <?php foreach ($guru as $value) { ?>
                                <option value="<?= $value->nip ?>"> <?= $value->nama ?> </option>
                              <?php } ?>  
                              </select>
                             <div class="invalid-feedback" id="er_nama_mapel">Nama Kelas Tidak boleh Kosong</div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Nama Mapel</label>
                            <div class="col-md-10">
                              <select class="form-control" name="id_mapel" id="id_mapel">
                                <option value=" ">Pilih</option>
                                <?php foreach ($mapel as $value) { ?>
                                <option value="<?= $value->id ?>"> <?= $value->nama_mapel ?> </option>
                              <?php } ?>  
                              </select>
                            </div>
                        </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="btn_cancel" data-dismiss="modal">Cancel</button>
                    <button type="button" type="submit" id="btn_save" class="btn btn-primary">Save</button>
                  </div>
                </form>
                </div>
              </div>
            </div>

            <!-- MODAL EDIT -->
        <form>
            <div class="modal fade" id="Modal_Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                        <input type="hidden" name="id" id="id_edit" class="form-control">
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Nama Guru</label>
                            <div class="col-md-10">
                             <select class="form-control" name="nip" id="nip_edit">
                                <option value=" ">Pilih</option>
                              </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Mata Pelajaran</label>
                            <div class="col-md-10">
                              <select class="form-control" name="id_mapel" id="mapel_edit">
                                <option value=" ">Pilih</option>
                              </select>
                            </div>
                        </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="btn_close" data-dismiss="modal">Close</button>
                    <button type="button" type="submit" id="btn_update" class="btn btn-primary">Update</button>
                  </div>
                </div>
              </div>
            </div>
            </form>
        <!--END MODAL EDIT-->



            <!--MODAL DELETE-->
         <form>
            <div class="modal fade" id="Modal_Delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                       <strong>Are you sure to delete this record?</strong>
                  </div>
                  <div class="modal-footer">
                    <input type="hidden" name="id" id="id" class="form-control">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button type="button" type="submit" id="btn_delete" class="btn btn-primary">Yes</button>
                  </div>
                </div>
              </div>
            </div>
            </form>
        <!--END MODAL DELETE-->

<script type="text/javascript" src="<?php echo base_url().'assets/dataTables/js/jquery-3.2.1.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/dataTables/js/jquery.dataTables.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/dataTables/js/dataTables.bootstrap4.js'?>"></script>

<script async='async' type="text/javascript">

    show_list();
   // $('#table-1').dataTable();

      function show_list(){
        $.ajax({
            type  : 'ajax',
            url   : '<?php echo site_url('guru_mapel/list')?>',
            async : false,
            dataType : 'json',
            success : function(data){
                var no = 1;
                var html = '';
                var i;
                for(i=0; i<data.length; i++){
                    html += '<tr>'+
                          '<td>'+ no++ +'</td>'+
                            '<td>'+data[i].nama+'</td>'+
                            '<td>'+data[i].nip+'</td>'+
                            '<td>'+data[i].nama_mapel+'</td>'+
                            '<td>'+
                                    '<a href="javascript:void(0);" class="btn btn-info btn-sm item_edit" data-id="'+data[i].id+'" data-nip="'+data[i].nip+'" data-mapel="'+data[i].mapel+'">Edit</a>'+' '+
                                    '<a href="javascript:void(0);" class="btn btn-danger btn-sm item_delete" data-id="'+data[i].id+'">Delete</a>'+
                                '</td>'+
                            '</tr>';
                }
                $('#show_list').html(html);
            }

        });
    }

           
        //get data for update record
        $('#show_list').on('click','.item_edit',function(){
            var id = $(this).data('id');
            var nip = $(this).data('nip');
            var mapel = $(this).data('mapel');
            
            $('#Modal_Edit').modal('show');
            $('[name="id"]').val(id);
            $.ajax({
                    url : "<?php echo site_url('guru_mapel/get_nip');?>",
                    async : true,
                    dataType : 'json',
                    success: function(data){
                         
                        $('#nip_edit').empty();
                        $.each(data, function(key, value) {
                            if(nip==value.nip){
                             $('#nip_edit').append('<option value="'+ value.nip +'" selected>'+ value.nama +'</option>').trigger('change');
                            }else{
                              $('#nip_edit').append('<option value="'+ value.nip +'">'+ value.nama +'</option>');
                            }
                        });
                    }
                });
                

            $.ajax({
                    url : "<?php echo site_url('guru_mapel/get_mapel');?>",
                    method : "POST",
                    async : true,
                    dataType : 'json',
                    success: function(data){
                         
                        $('#mapel_edit').empty();
                        $.each(data, function(key, value) {
                            if(mapel==value.id){
                             $('#mapel_edit').append('<option value="'+ value.id +'" selected>'+ value.nama_mapel +'</option>').trigger('change');
                            }else{
                              $('#mapel_edit').append('<option value="'+ value.id +'">'+ value.nama_mapel +'</option>');
                            }
                        });
                    }
                });
                return false;
        });

        //update record to database
         $('#btn_update').on('click',function(){
            var id = $('#id_edit').val();
            var nip = $('#nip_edit').val();
            var id_mapel = $('#mapel_edit').val();
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('guru_mapel/update')?>",
                dataType : "JSON",
                data : {id:id , nip:nip, id_mapel:id_mapel},
                success: function(data){
                    $('[name="id"]').val("");
                    $('#nip_edit').empty();
                    $('#mapel_edit').empty();
                    $('#Modal_Edit').modal('hide');
                    show_list();
                }
            });
            return false;
        });

        //Save product
        $('#btn_save').on('click',function(){
            var nip = $('#id_guru').val();
            var id_mapel        = $('#id_mapel').val();
            if (nip == '') {
              
            }else{
                          $.ajax({
                type : "POST",
                url  : "<?php echo site_url('guru_mapel/save')?>",
                dataType : "JSON",
                data : {nip:nip, id_mapel:id_mapel},
                success: function(data){
                    $('#Modal_Add').modal('hide');
                    show_list();
                }
            });
            return false;

            }

        });

        //get data for delete record
        $('#show_list').on('click','.item_delete',function(){
            var id_mapel = $(this).data('id');
            
            $('#Modal_Delete').modal('show');
            $('[name="id"]').val(id_mapel);
        });

        //delete record to database
         $('#btn_delete').on('click',function(){
            var id = $('#id').val();
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('guru_mapel/delete')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                    $('[name="id"]').val("");
                    $('#Modal_Delete').modal('hide');
                    show_list();
                }
            });
            return false;
        });
</script>

<?php $this->load->view('_partials/footer'); ?>