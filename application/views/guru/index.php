<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('_partials/header');
?>
 <!--  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/dataTables/css/jquery.dataTables.css'?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/dataTables/css/dataTables.bootstrap4.css'?>"> -->
  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Data Guru</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
          <div class="breadcrumb-item"><a href="#">Data Master</a></div>
          <div class="breadcrumb-item">Mata Pelajaran</div>
        </div>
      </div>

      <div class="section-body">
        <a href="#" data-toggle="modal" data-target="#Modal_Add" class="btn btn-icon icon-left btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>

        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-striped" id="table-1">
                    <thead>                                 
                      <tr>
                        <th class="text-center">
                          #
                        </th>
                        <th>Nama</th>
                        <th>nip</th>
                        <th>Golongan</th>
                        <th>Pendidikan Terakhir</th>
                        <th>NO HP</th>
                        <th>Tanggal SK</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody id="show_data">                                 

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <div class="modal fade" id="Modal_Add" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Data Guru</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form>
          <div class="modal-body">
            <div class="row">
              <div class="col-12 col-md-6 col-lg-6">
                <div class="card">
                  <div class="card-body">
                    <div class="form-group">
                      <label>Nama</label>
                      <input type="text" class="form-control" name="nama" id="nama">
                    </div>
                    <div class="form-group">
                      <label>Email</label>
                      <input type="text" class="form-control" name="email" id="email">
                    </div>
                    <div class="form-group">
                      <label>Alamat</label>
                      <input type="text" class="form-control" name="alamat" id="alamat">
                    </div>
                    <div class="form-group">
                      <label>Tanggal Lahir</label>
                      <input type="date" class="form-control" name="tanggal_lahir" id="tanggal_lahir">
                    </div>
                    <div class="form-group">
                      <label>Tempat Lahir</label>
                      <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir">
                    </div>
                    <div class="form-group">
                      <label>Jenis Kelamin</label>
                      <input type="text" class="form-control" name="jenis_kelamin" id="jenis_kelamin">
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-12 col-md-6 col-lg-6">
                <div class="card">
                  <div class="card-body">
                    <div class="form-group">
                      <label>nip</label>
                      <input type="text" class="form-control" name="nip" id="nip">
                    </div>
                    <div class="form-group">
                      <label>Golongan</label>
                      <input type="text" class="form-control" name="golongan" id="golongan">
                    </div>
                    <div class="form-group">
                      <label>Pendidikan Terakhir</label>
                      <input type="text" class="form-control" name="pendidikan_terakhir" id="pendidikan_terakhir">
                    </div>
                    <div class="form-group">
                      <label>No. HP</label>
                      <input type="text" class="form-control" name="telfon" id="telfon">
                    </div>
                    <div class="form-group">
                      <label>tanggal_sk</label>
                      <input type="date" class="form-control" name="tanggal_sk" id="tanggal_sk">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" id="btn_cancel" data-dismiss="modal">Cancel</button>
            <button type="button" type="submit" id="btn_save" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- MODAL EDIT -->
  <form>
    <div class="modal fade" id="Modal_Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Edit Product</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="modal-body">
              <div class="row">
                <div class="col-12 col-md-6 col-lg-6">
                  <div class="card">
                    <div class="card-body">
                      <div class="form-group">
                        <label>Nama</label>
                        <input type="text" class="form-control" name="nama" id="nama_edit">
                        <input type="hidden" class="form-control" name="id" id="id_edit">
                      </div>
                      <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" name="email" id="email_edit">
                      </div>
                      <div class="form-group">
                        <label>Alamat</label>
                        <input type="text" class="form-control" name="alamat" id="alamat_edit">
                      </div>
                      <div class="form-group">
                        <label>Tanggal Lahir</label>
                        <input type="date" class="form-control" name="tanggal_lahir" id="tanggal_lahir_edit">
                      </div>
                      <div class="form-group">
                        <label>Tempat Lahir</label>
                        <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir_edit">
                      </div>
                      <div class="form-group">
                        <label>Jenis Kelamin</label>
                        <input type="text" class="form-control" name="jenis_kelamin" id="jenis_kelamin_edit">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-6 col-lg-6">
                  <div class="card">
                    <div class="card-body">
                      <div class="form-group">
                        <label>nip</label>
                        <input type="text" class="form-control" name="nip" id="nip_edit">
                      </div>
                      <div class="form-group">
                        <label>Golongan</label>
                        <input type="text" class="form-control" name="golongan" id="golongan_edit">
                      </div>
                      <div class="form-group">
                        <label>Pendidikan Terakhir</label>
                        <input type="text" class="form-control" name="pendidikan_terakhir" id="pendidikan_terakhir_edit">
                      </div>
                      <div class="form-group">
                        <label>No. HP</label>
                        <input type="text" class="form-control" name="telfon" id="telfon_edit">
                      </div>
                      <div class="form-group">
                        <label>tanggal_sk</label>
                        <input type="date" class="form-control" name="tanggal_sk" id="tanggal_sk_edit">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" id="btn_close" data-dismiss="modal">Close</button>
            <button type="button" type="submit" id="btn_update" class="btn btn-primary">Update</button>
          </div>
        </div>
      </div>
    </div>
  </form>
  <!--END MODAL EDIT-->



  <!--MODAL DELETE-->
  <form>
    <div class="modal fade" id="Modal_Delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Delete Product</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
           <strong>Are you sure to delete this record?</strong>
         </div>
         <div class="modal-footer">
          <input type="hidden" name="id" id="id" class="form-control">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
          <button type="button" type="submit" id="btn_delete" class="btn btn-primary">Yes</button>
        </div>
      </div>
    </div>
  </div>
</form>
<!--END MODAL DELETE-->

<script type="text/javascript" src="<?php echo base_url().'assets/dataTables/js/jquery-3.2.1.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/dataTables/js/jquery.dataTables.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/dataTables/js/dataTables.bootstrap4.js'?>"></script>

<script async='async' type="text/javascript">

  show_data();
  $('#btn_close').on('click',function(){
    $('[name="id"]').val('');
    $('[name="nama_mapel"]').val('');
    $('[name="kode_mapel"]').val('');
  });
  $('#btn_cancel').on('click',function(){
    $('[name="id"]').val('');
    $('[name="nama_mapel"]').val('');
    $('[name="kode_mapel"]').val('');
  });
   // $('#table-1').dataTable();

   function show_data(){
    $.ajax({
      type  : 'ajax',
      url   : '<?php echo site_url('guru/list')?>',
      async : false,
      dataType : 'json',
      success : function(data){
        var no = 1;
        var html = '';
        var i;
        for(i=0; i<data.length; i++){
          html += '<tr>'+
          '<td>'+ no++ +'</td>'+
          '<td>'+data[i].nama+'</td>'+
          '<td>'+data[i].nip+'</td>'+
          '<td>'+data[i].golongan+'</td>'+
          '<td>'+data[i].pendidikan_terakhir+'</td>'+
          '<td>'+data[i].telfon+'</td>'+
          '<td>'+data[i].tanggal_sk+'</td>'+
          '<td>'+
          '<a href="javascript:void(0);" class="btn btn-secondary btn-sm item_edit" data-id="'+data[i].id+'" data-nama_mapel="'+data[i].nama_mapel+'" data-kode_mapel="'+data[i].kode_mapel+'">Detail</a>'+' '+

          '<a href="javascript:void(0);" class="btn btn-info btn-sm item_edit" data-id="'+data[i].id+'" data-nama="'+data[i].nama+'" data-email="'+data[i].email+'" data-tanggal_lahir="'+data[i].tanggal_lahir+'" data-tempat_lahir="'+data[i].tempat_lahir+'" data-alamat="'+data[i].alamat+'" data-golongan="'+data[i].golongan+'" data-nip="'+data[i].nip+'" data-jenis_kelamin="'+data[i].jenis_kelamin+'" data-pendidikan_terakhir="'+data[i].pendidikan_terakhir+'" data-telfon="'+data[i].telfon+'" data-tanggal_sk="'+data[i].tanggal_sk+'">Edit</a>'+' '+

          '<a href="javascript:void(0);" class="btn btn-danger btn-sm item_delete" data-id="'+data[i].id+'">Delete</a>'+
          '</td>'+
          '</tr>';
        }
        $('#show_data').html(html);
      }

    });
  }


        //get data for update record
        $('#show_data').on('click','.item_edit',function(){
          var id = $(this).data('id');
          var nama = $(this).data('nama');
          var email        = $(this).data('email');
          var tanggal_lahir = $(this).data('tanggal_lahir');
          var tempat_lahir        = $(this).data('tempat_lahir');
          var alamat        = $(this).data('alamat');
          var golongan = $(this).data('golongan');
          var nip        = $(this).data('nip');
          var jenis_kelamin = $(this).data('jenis_kelamin');
          var pendidikan_terakhir        = $(this).data('pendidikan_terakhir');
          var telfon = $(this).data('telfon');
          var tanggal_sk        = $(this).data('tanggal_sk');

          $('#Modal_Edit').modal('show');
          $('[name="id"]').val(id);
          $('[name="nama"]').val(nama);
          $('[name="email"]').val(email);
          $('[name="tanggal_lahir"]').val(tanggal_lahir);
          $('[name="tempat_lahir"]').val(tempat_lahir);
          $('[name="alamat"]').val(alamat);
          $('[name="golongan"]').val(golongan);
          $('[name="nip"]').val(nip);
          $('[name="jenis_kelamin"]').val(jenis_kelamin);
          $('[name="pendidikan_terakhir"]').val(pendidikan_terakhir);
          $('[name="telfon"]').val(telfon);
          $('[name="tanggal_sk"]').val(tanggal_sk);
        });

        //update record to database
        $('#btn_update').on('click',function(){
          var id = $('#id_edit').val();
          var nama = $('#nama_edit').val();
          var email        = $('#email_edit').val();
          var tanggal_lahir = $('#tanggal_lahir_edit').val();
          var tempat_lahir        = $('#tempat_lahir_edit').val();
          var alamat        = $('#alamat_edit').val();
          var golongan = $('#golongan_edit').val();
          var nip        = $('#nip_edit').val();
          var jenis_kelamin = $('#jenis_kelamin_edit').val();
          var pendidikan_terakhir        = $('#pendidikan_terakhir_edit').val();
          var telfon = $('#telfon_edit').val();
          var tanggal_sk        = $('#tanggal_sk_edit').val();
          $.ajax({
            type : "POST",
            url  : "<?php echo site_url('guru/update')?>",
            dataType : "JSON",
            data : {id:id , nama:nama, email:email, tanggal_lahir:tanggal_lahir, tempat_lahir:tempat_lahir, alamat:alamat, golongan:golongan, nip:nip, jenis_kelamin:jenis_kelamin, pendidikan_terakhir:pendidikan_terakhir, telfon:telfon, tanggal_sk:tanggal_sk},
            success: function(data){
              $('[name="id"]').val("");
                $('[name="nama"]').val("");
                $('[name="email"]').val("");
                $('[name="tanggal_lahir"]').val("");
                $('[name="tempat_lahir"]').val("");
                $('[name="alamat"]').val("");
                $('[name="golongan"]').val("");
                $('[name="nip"]').val("");
                $('[name="jenis_kelamin"]').val("");
                $('[name="pendidikan_terakhir"]').val("");
                $('[name="telfon"]').val("");
                $('[name="tanggal_sk"]').val("");              $('#Modal_Edit').modal('hide');
              show_data();
            }
          });
          return false;
        });

        //Save product
        $('#btn_save').on('click',function(){
          var nama = $('#nama').val();
          var email        = $('#email').val();
          var tanggal_lahir = $('#tanggal_lahir').val();
          var tempat_lahir        = $('#tempat_lahir').val();
          var alamat        = $('#alamat').val();
          var golongan = $('#golongan').val();
          var nip        = $('#nip').val();
          var jenis_kelamin = $('#jenis_kelamin').val();
          var pendidikan_terakhir        = $('#pendidikan_terakhir').val();
          var telfon = $('#telfon').val();
          var tanggal_sk        = $('#tanggal_sk').val();
          if (nama == '') {

          }else{
            $.ajax({
              type : "POST",
              url  : "<?php echo site_url('guru/save')?>",
              dataType : "JSON",
              data : {nama:nama, email:email, tanggal_lahir:tanggal_lahir, tempat_lahir:tempat_lahir, alamat:alamat, golongan:golongan, nip:nip, jenis_kelamin:jenis_kelamin, pendidikan_terakhir:pendidikan_terakhir, telfon:telfon, tanggal_sk:tanggal_sk},
              success: function(data){
                $('[name="nama"]').val("");
                $('[name="email"]').val("");
                $('[name="tanggal_lahir"]').val("");
                $('[name="tempat_lahir"]').val("");
                $('[name="alamat"]').val("");
                $('[name="golongan"]').val("");
                $('[name="nip"]').val("");
                $('[name="jenis_kelamin"]').val("");
                $('[name="pendidikan_terakhir"]').val("");
                $('[name="telfon"]').val("");
                $('[name="tanggal_sk"]').val("");
                $('#Modal_Add').modal('hide');
                show_data();
              }
            });
            return false;

          }

        });

        //get data for delete record
        $('#show_data').on('click','.item_delete',function(){
          var id_mapel = $(this).data('id');

          $('#Modal_Delete').modal('show');
          $('[name="id"]').val(id_mapel);
        });

        //delete record to database
        $('#btn_delete').on('click',function(){
          var id = $('#id').val();
          $.ajax({
            type : "POST",
            url  : "<?php echo site_url('guru/delete')?>",
            dataType : "JSON",
            data : {id:id},
            success: function(data){
              $('[name="id"]').val("");
              $('#Modal_Delete').modal('hide');
              show_data();
            }
          });
          return false;
        });
      </script>

      <?php $this->load->view('_partials/footer'); ?>